package org.clebi067.tradesparksc.average

import org.apache.spark.internal.Logging
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.types.StringType
import org.clebi067.tradesparksc.ReaderStream
import org.clebi067.tradesparksc.WriterStream

class TradesAvgJoin(spark: SparkSession, left: ReaderStream, right: ReaderStream, slideSecs: Int)
    extends Logging
    with ReaderStream {

  override def read(): Dataset[Row] = {
    import spark.implicits._

    return left
      .read()
      .join(
        right.read().withColumnRenamed("symbol", "rsymbol"),
        expr(s"""
    symbol = rsymbol AND
    tradeTime > end AND
    tradeTime < end + interval ${slideSecs} seconds
    """),
        joinType = "inner"
      )
  }

}
