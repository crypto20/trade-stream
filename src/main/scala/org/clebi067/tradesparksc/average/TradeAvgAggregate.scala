// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.clebi067.tradesparksc.average

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DoubleType
import org.clebi067.tradesparksc.ReaderStream
import org.clebi067.tradesparksc.WriterStream

/** TradeAggregateStream does all aggregation for the trade stream.
  * It will read an input stream and can output to multiple streams.
  *
  * @param spark spark session.
  * @param windowMins window in minutes.
  * @param slideSecs slide in seconds.
  * @param input input stream.
  * @param outputs array of output streams.
  */
class TradeAverageStream(
    spark: SparkSession,
    windowMins: Int,
    slideSecs: Int,
    input: ReaderStream,
    outputs: Array[WriterStream]
) {
  def aggregate(): Unit = {
    import spark.implicits._

    val ds = input
      .read()
      .groupBy(
        window($"tradeTime", s"${windowMins} minutes", s"${slideSecs} seconds"),
        $"symbol"
      )
      .agg(
        count("price").as("points"),
        avg("price").as("price_avg"),
        last($"price").as("price_last"),
        stddev("price").as("price_stddev"),
        corr($"tradeTime".cast(DoubleType), $"price").as("price_corr"),
        avg($"tradeTime".cast(DoubleType)).as("tradeTime_avg"),
        variance($"price").as("price_var"),
        variance($"tradeTime".cast(DoubleType)).as("tradeTime_var")
      )
      .withColumn("window_start", $"window.start")
      .withColumn("window_end", $"window.end")
      .drop("window")
      .withColumn("price_a", sqrt($"price_var" / $"tradeTime_var") * $"price_corr")
      .withColumn("price_b", $"price_avg" - $"price_a" * $"tradeTime_avg")
      .drop("tradeTime_avg")

    outputs.foreach(output => output.write(ds))
  }

  def await(): Unit = outputs.foreach(output => output.await())

}
