package org.clebi067.tradesparksc.average

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.TimestampType
import org.clebi067.tradesparksc.ReaderStream
import org.clebi067.tradesparksc.binance.KafkaTradeStream

object TradeAvgKafkaInput {
  val STREAM_WATERMARK = "30 seconds"
}

class TradeAvgKafkaInput(
    spark: SparkSession,
    url: String,
    topicName: String
) extends ReaderStream {
  val avgSchema = new StructType()
    .add("symbol", "string")
    .add("points", "int")
    .add("start", "long")
    .add("end", "long")
    .add("price_avg", "double")
    .add("price_stddev", "double")
    .add("price_corr", "double")
    .add("price_a", "double")
    .add("price_b", "double")

  /** Creates the stream.
    *
    * @return the dataset.
    */
  override def read(): Dataset[Row] = {
    import spark.implicits._

    return spark.readStream
      .format("kafka")
      .option("subscribe", topicName)
      .option("kafka.bootstrap.servers", url)
      .option("startingOffsets", "latest")
      .load()
      .select('value cast "string" as "data")
      .select(from_json($"data", avgSchema) as "data")
      .select("data.*")
      .withColumn(
        "start",
        from_unixtime($"start" cast "bigint").cast(TimestampType)
      )
      .withColumn(
        "end",
        from_unixtime($"end" cast "bigint").cast(TimestampType)
      )
      .withWatermark("end", TradeAvgKafkaInput.STREAM_WATERMARK)
  }
}
