package org.clebi067.tradesparksc.average

import org.apache.spark.internal.Logging
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.clebi067.tradesparksc.ReaderStream

class TradesAvgSignal(spark: SparkSession, ds: ReaderStream) extends Logging with ReaderStream {
  override def read(): Dataset[Row] = {
    import spark.implicits._

    return ds
      .read()
      .drop("rsymbol", "eventTime", "eventType", "tradeId", "buyerId", "sellerId", "isMarketMaker")
      .withColumn("price_expect", ($"end" cast "bigint") * $"price_a" + $"price_b")
      .withColumn("price_up", $"price" > $"price_expect" + $"price_stddev")
      .withColumn("price_down", $"price" < $"price_expect" - $"price_stddev")
  }
}
