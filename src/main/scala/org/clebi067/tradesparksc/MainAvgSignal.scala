package org.clebi067.tradesparksc

import org.apache.spark.internal.Logging
import org.apache.spark.sql.RuntimeConfig
import org.apache.spark.sql.SparkSession
import org.clebi067.tradesparksc.average.TradeAvgKafkaInput
import org.clebi067.tradesparksc.average.TradesAvgJoin
import org.clebi067.tradesparksc.average.TradesAvgSignal
import org.clebi067.tradesparksc.binance.KafkaTradeStream

import scala.util.Properties

object MainAvgSignal extends Logging {
  val env = Properties.envOrElse("BINANCE_ENV", "dev")
  if (env.length() < 1) {
    throw new IllegalArgumentException("env is empty")
  }
  val kafkaUrl = Properties.envOrElse("BINANCE_STREAM_KAFKA_URL", "localhost:9092");

  def getAppConf(conf: RuntimeConfig): (String, Int, Int, String) = {
    val pair = conf.get("spark.bin.pair")
    val windowMins = conf.get("spark.bin.window", "10").toInt
    val slideSecs = conf.get("spark.bin.slide", "30").toInt
    val topicPrefix = if (env == "prod") "" else s"${env}_"
    logDebug(s"topic prefix: ${topicPrefix}")
    val topicAvgName = s"${pair}_avg_${windowMins}m${slideSecs}s"
    return (pair, windowMins, slideSecs, topicAvgName)
  }

  def main(args: Array[String]) {
    val spark = SparkSession
      .builder()
      .appName("BinanceStreamStrut")
      .config("spark.sql.streaming.checkpointLocation", "/tmp/blockchain-streaming/sql-streaming-checkpoint")
      .config("spark.streaming.backpressure.enabled", true)
      .config("spark.streaming.kafka.consumer.poll.ms", 512)
      .getOrCreate()

    val (pair, windowMins, slideSecs, topicAvgName) = getAppConf(spark.conf)
    logInfo(s"processing pair: ${pair} with window: ${windowMins} minutes with sliding ${slideSecs} seconds");
    logInfo(s"topic average: ${topicAvgName}")

    spark.sparkContext.setLogLevel("WARN")

    val inputTradeStream = new KafkaTradeStream(spark, kafkaUrl, pair)
    val inputAvgStream = new TradeAvgKafkaInput(spark, kafkaUrl, topicAvgName)

    val inputJoinStream = new TradesAvgJoin(spark, inputAvgStream, inputTradeStream, slideSecs)
    val inputSignalStream = new TradesAvgSignal(spark, inputJoinStream)

    val outputJoinStream = new TradeConsoleOutput(spark)
    outputJoinStream.write(inputSignalStream.read())

    outputJoinStream.await()
  }
}
