// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.clebi067.tradesparksc.average

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.types.StringType
import org.clebi067.tradesparksc.WriterStream

/**
  * Output trade stream to kafka topic.
  *
  * @param spark spark session.
  * @param url kafka url.
  * @param topicName topic name.
  */
class TradeAverageKafkaOutput(spark: SparkSession, url: String, topicName: String) extends WriterStream {

  var stream: StreamingQuery = null

  override def write(ds: Dataset[Row]): Unit = {
    import spark.implicits._

    stream = ds
      .select(
        $"window_end".cast(StringType).as("key"),
        to_json(
          struct(
            $"symbol",
            $"points",
            $"window_start".cast(LongType).as("start"),
            $"window_end".cast(LongType).as("end"),
            $"price_avg",
            $"price_stddev",
            $"price_corr",
            $"price_a",
            $"price_b"
          )
        ).as("value")
      )
      .writeStream
      .outputMode("append")
      .format("kafka")
      .option("kafka.bootstrap.servers", url)
      .option("topic", topicName)
      .start()
  }

  override def await(): Unit = {
    if (stream == null) {
      throw new IllegalStateException("stream is not defined")
    }
    stream.awaitTermination()
  }

}
