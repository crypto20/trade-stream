// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.clebi067.tradesparksc

import org.apache.log4j.Logger
import org.apache.spark.internal.Logging
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.RuntimeConfig
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.TimestampType
import org.clebi067.tradesparksc.average.TradeAverageStream
import org.clebi067.tradesparksc.average.TradeAverageKafkaOutput
import org.clebi067.tradesparksc.binance.KafkaTradeStream

import java.util.concurrent.TimeUnit
import scala.util.Properties

object MainAverage extends Logging {

  val env = Properties.envOrElse("BINANCE_ENV", "dev")
  if (env.length() < 1) {
    throw new IllegalArgumentException("env is empty")
  }
  val kafkaUrl = Properties.envOrElse("BINANCE_STREAM_KAFKA_URL", "localhost:9092");

  def getAppConf(conf: RuntimeConfig): (String, Int, Int, String) = {
    val pair = conf.get("spark.bin.pair")
    val windowMins = conf.get("spark.bin.window", "10").toInt
    val slideSecs = conf.get("spark.bin.slide", "30").toInt
    val topicPrefix = if (env == "prod") "" else s"${env}_"
    logDebug(s"topic prefix: ${topicPrefix}")
    val topicAvgName = s"${topicPrefix}${pair}_avg_${windowMins}m${slideSecs}s"
    return (pair, windowMins, slideSecs, topicAvgName)
  }

  def main(args: Array[String]) {
    val spark = SparkSession
      .builder()
      .appName("BinanceStreamStruct")
      .config("spark.sql.streaming.checkpointLocation", "/tmp/blockchain-streaming/sql-streaming-checkpoint")
      .config("spark.streaming.backpressure.enabled", true)
      .config("spark.streaming.kafka.consumer.poll.ms", 512)
      .getOrCreate()

    val (pair, windowMins, slideSecs, topicAvgName) = getAppConf(spark.conf)
    logInfo(s"processing pair: ${pair} with window: ${windowMins} minutes with sliding ${slideSecs} seconds");
    logInfo(s"topic average: ${topicAvgName}")

    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")

    val inputStream = new KafkaTradeStream(spark, kafkaUrl, pair)
    val aggStream = new TradeAverageStream(
      spark,
      windowMins,
      slideSecs,
      inputStream,
      Array(new TradeAverageKafkaOutput(spark, kafkaUrl, topicAvgName))
    )

    aggStream.aggregate()
    aggStream.await()

  }
}
