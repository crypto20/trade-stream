// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.clebi067.tradesparksc

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.StreamingQuery

/** TradeConsoleOutput outputs the trade stream to console.
  *
  * @param spark spark session.
  */
class TradeConsoleOutput(spark: SparkSession) extends WriterStream {

  var stream: StreamingQuery = null

  override def write(ds: Dataset[Row]): Unit = {
    import spark.implicits._

    stream = ds
      .writeStream
      .outputMode("append")
      .option("truncate", false)
      .format("console")
      .start()
  }

  override def await(): Unit = {
    if (stream == null) {
      throw new IllegalStateException("stream is not defined")
    }
    stream.awaitTermination()
  }

}
