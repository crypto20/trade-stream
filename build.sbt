
scalaVersion := "2.12.13"

organization := "org.clebi067"
name := "tradesparksc"
version := "1.0"

val sparkVersion = "3.1.1"

libraryDependencies +=  "org.apache.spark" %% "spark-core" % sparkVersion % "provided"
libraryDependencies +=  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided"
libraryDependencies +=  "org.apache.spark" %% "spark-streaming" % sparkVersion % "provided"
libraryDependencies +=  "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion

assemblyMergeStrategy in assembly := {
  case PathList("org", "apache", "spark", "unused", "UnusedStubClass.class") => MergeStrategy.first
  case x => (assemblyMergeStrategy in assembly).value(x)
}
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
