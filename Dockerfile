FROM hseeberger/scala-sbt:graalvm-ce-21.1.0-java11_1.5.1_2.12.13 as builder

COPY . .

RUN sbt assembly

FROM registry.gitlab.com/crypto20/bitnami-docker-spark:master

RUN mkdir /opt/applications
COPY --from=builder /root/target/scala-2.12/tradesparksc-assembly-1.0.jar /opt/applications/trade-spark.jar
