// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.clebi067.tradesparksc.binance

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.TimestampType
import org.clebi067.tradesparksc.ReaderStream

object KafkaTradeStream {
  val DIVISION_TIMESTAMP_MS = 1000
  val CSV_SEP = ","
  val STREAM_WATERMARK = "5 seconds"
}

/** KafkaTradeStream consumes messages from trade kafka topic as stream.
  *
  * @param spark spark session.
  * @param url url of kafka.
  * @param pair the concerned pair.
  * @param checkpointName the name of checkpoint.
  */
class KafkaTradeStream(spark: SparkSession, url: String, pair: String) extends ReaderStream {
  val tradeSchema = new StructType()
    .add("eventType", "string")
    .add("eventTime", "long")
    .add("symbol", "string")
    .add("tradeId", "long")
    .add("price", "double")
    .add("quantity", "double")
    .add("buyerId", "long")
    .add("sellerId", "long")
    .add("tradeTime", "long")
    .add("isMarketMaker", "boolean")

  /** Creates the stream.
    *
    * @return the dataset.
    */
  override def read(): Dataset[Row] = {
    import spark.implicits._

    return spark.readStream
      .format("kafka")
      .option("subscribe", s"trade-${pair}")
      .option("kafka.bootstrap.servers", url)
      .option("startingOffsets", "latest")
      .load()
      .select('value cast "string" as "data")
      .select(from_csv(col("data"), tradeSchema, Map("sep" -> KafkaTradeStream.CSV_SEP)) as "data")
      .select("data.*")
      .withColumn(
        "eventTime",
        from_unixtime(($"eventTime" cast "bigint") / KafkaTradeStream.DIVISION_TIMESTAMP_MS).cast(TimestampType)
      )
      .withColumn(
        "tradeTime",
        from_unixtime(($"tradeTime" cast "bigint") / KafkaTradeStream.DIVISION_TIMESTAMP_MS).cast(TimestampType)
      )
      .withWatermark("tradeTime", KafkaTradeStream.STREAM_WATERMARK)
  }
}
